import os
import yaml

basedir = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(basedir, 'config.yaml')) as f:
    config_data = yaml.safe_load(f)


class Config(object):
    SERVER_NAME=config_data.get("SERVER_NAME", "localhost")
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or config_data.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = config_data.get("SQLALCHEMY_TRACK_MODIFICATIONS")
