# Vineale URL Shortener
## How to install
1. Install dependencies

`$ sudo apt install git nginx gunicorn python3-venv python3-pip libaugeas0`
2. Clone the repository

`$ git clone https://gitlab.com/sr2c/vineale.git`
3. Change directory to the repo and copy to /var/www/vineale
```
$ cd vineale
# cp -r * /var/www/vineale
```
4. Add systemd service at /etc/systemd/system/vineale.conf with the following information:
```
[Unit]
Description=Vineale URL shortener
After=network.target

[Service]
User=root
Group=www-data
WorkingDirectory=/var/www/vineale/
Environment="PATH=/var/www/vineale/.venv/bin"
ExecStart=/var/www/vineale/.venv/bin/gunicorn --workers 2 --bind unix:/var/www/vineale/vineale.sock vineale:app
Restart=always

[Install]
WantedBy=multi-user.target
```
5. Add nginx configuration to sites-available as vineale.conf
```
server {
    server_name go.sr2.uk;

    access_log /var/log/nginx/vineale.access.log;
    error_log /var/log/nginx/vineale.error.log;

    location / {
        include proxy_params;
        proxy_pass http://unix:/var/www/vineale/vineale.sock;
    }
}
```
6. Symbolically link from sites-enabled

`# ln -s /etc/nginx/sites-available/vineale.conf /etc/nginx/sites-enabled/vineale.conf`
7. Navigate to the application instance

`$ cd /var/www/vineale`
8. Remove any system installation of certbot if it is present

`# apt remove certbot`
9. Set up python virtual environment

`# python3 -m venv .venv`
10. Enter python virtual environment

`$ source .venv/bin/activate`
11. Install server dependencies with pip

`# pip install -r requirements.txt`
12. Run Certbot setup command for Nginx and follow any prompts given

`# certbot --nginx`