import re

# Values from Shields.io
ALIASES = {
    "gray": "grey",
    "lightgray": "lightgrey",
    "critical": "red",
    "important": "orange",
    "success": "brightgreen",
    "informational": "blue",
    "inactive": "lightgrey"
}

NAMED_COLORS = {
    "brightgreen": "#4c1",
    "green": "#97ca00",
    "yellow": "#dfb317",
    "yellowgreen": "#a4a61d",
    "orange": "#fe7d37",
    "red": "#e05d44",
    "blue": "#007ec6",
    "grey": "#555",
    "lightgrey": "#9f9f9f"
}


def process_color(color):
    if len(color) in [3, 6] and re.match("[0-9A-Fa-f]{3,6}", color):
        return "#" + color
    else:
        if color in ALIASES.keys():
            color = ALIASES[color]

        if color in NAMED_COLORS.keys():  # Not elif as the alias may lead to a named color
            color = NAMED_COLORS[color]

    return color
