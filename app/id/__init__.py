from flask import Blueprint, url_for, redirect
from flask.typing import ResponseValue

id_ = Blueprint("id", __name__, subdomain="id")

PRIMARY_ID_KEYS = [
    # Release 1.4.0, Ratified, Jun 2023
    "01",  # GTIN
    "8006",  # ITIP
    "8013",  # GMN
    "8010",  # CPID
    "414",  # Physical GLN
    "415",  # Invoicing party GLN
    "417",  # Party GLN
    "8017",  # Provider GSRN
    "8018",  # Recipient GSRN
    "255",  # GCN
    "00",  # SSCC
    "253",  # GTDI
    "401",  # GINC
    "402",  # GSIN
    "8003",  # GRAI
    "8004",  # GIAI
]

KEY_QUALIFIERS = [
    # Release 1.4.0, Ratified, Jun 2023
    "22",  # Consumer Product Variant
    "10",  # Batch/Lot identifier
    "21",  # GTIN Serial Number
    "8011",  # CPID Serial Number
    "254",  # GLN extension
    "8020",  # Payment Reference Number
    "8019",  # Service Relation Instance Number
    "235",  # third-party controlled serialised extension to GTIN
    "7040",  # GS1 UIC with Extension 1 and Importer Index
]


class GS1DigitalLinkSyntaxError(ValueError):
    message: str

    def __init__(self, message):
        super().__init__()
        self.message = message


@id_.route("/")
def front_page_id() -> ResponseValue:
    return redirect(url_for("go.front_page"))


@id_.route("/417/5060979190008")
def shortcut() -> ResponseValue:
    return redirect("https://www.sr2.uk/")


@id_.route("/414/5060979190022")
def shortcut_office() -> ResponseValue:
    return redirect("https://www.sr2.uk/office/")


@id_.route("/414/5060979190039")
def shortcut_deliveries() -> ResponseValue:
    return redirect("https://www.sr2.uk/deliveries/")


def client_error(message, status=400):
    return message, status


@id_.route("/<path:path>")
def digital_link(path):
    parts = path.split("/")
    try:
        if len(parts) % 2 != 0:
            raise ValueError("Number of values must match number of codes")
        elements = dict(zip(parts[::2], parts[1::2]))
        if elements.keys()[0] not in PRIMARY_ID_KEYS:
            raise ValueError("First element must be for a primary key")
    except ValueError as exc:
        return client_error(f"Invalid GS1 Digital Link URL: {exc.args[0]}")
