import logging

from flask import Flask

from app.extensions import db, migrate
from app.go import go
from app.id import id_
from app.test import test
from config import Config

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)
migrate.init_app(app, db)

app.register_blueprint(go)
app.register_blueprint(id_)
app.register_blueprint(test)

if __name__ == '__main__':
    app.run(debug=True, host="127.0.0.1", port=5001)
