import requests
from flask import Response, send_file


def return_image_from_web(url, mimetype):
    image = requests.get(url)
    if image.status_code in [200]:
        return Response(image.content, mimetype=mimetype)
    else:
        return send_file("../static/err-no-image-found.png", "image/png")
