from flask import Blueprint, render_template

test = Blueprint("test", __name__, subdomain="test")


@test.route("/", methods=['GET', 'POST'])
def hello_world():
    return render_template("test.html")
